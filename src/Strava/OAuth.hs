{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Strava.OAuth where

import Control.Monad.IO.Class (MonadIO (..))
-- import Control.Monad.Trans.Except (ExceptT (..), throwE)
import Data.Aeson (FromJSON, eitherDecode)
import qualified Data.ByteString as B
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as Char8
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.Lazy.Char8 as BSL
import Data.List.Split
import Data.Maybe (fromJust, isJust)
import Data.Text.Encoding
import qualified Data.Text.Encoding as T
import Lens.Micro (over)
import Network.HTTP.Conduit
import Network.HTTP.Simple (getResponseBody, getResponseHeader)
import qualified Network.HTTP.Types as HT
import Network.OAuth.OAuth2 (APIAuthenticationMethod (..), AccessToken (..), PostBody, uriToRequest)
import URI.ByteString (URI, URIRef, queryL, queryPairsL)

authGetBSWithAuthMethod ::
  (MonadIO m) =>
  -- | Specify the way that how to append the 'AccessToken' in the request
  APIAuthenticationMethod ->
  -- | HTTP connection manager.
  Manager ->
  AccessToken ->
  URI ->
  -- | Response as ByteString
  m (StravaResponse (Either BSL.ByteString BSL.ByteString))
authGetBSWithAuthMethod authTypes manager token url = do
  let appendToUrl = AuthInRequestQuery == authTypes
  let appendToHeader = AuthInRequestHeader == authTypes
  let uri = if appendToUrl then url `appendAccessToken` token else url
  let upReq = updateRequestHeaders (if appendToHeader then Just token else Nothing) . setMethod HT.GET
  req <- liftIO $ uriToRequest uri
  authRequest req upReq manager

-- | Set several header values:
--   + userAgennt    : `hoauth2`
--   + accept        : `application/json`
--   + authorization : 'Bearer' `xxxxx` if 'AccessToken' provided.
updateRequestHeaders :: Maybe AccessToken -> Request -> Request
updateRequestHeaders t req =
  let bearer = [(HT.hAuthorization, "Bearer " `BS.append` T.encodeUtf8 (atoken (fromJust t))) | isJust t]
      headers = bearer ++ defaultRequestHeaders ++ requestHeaders req
   in req{requestHeaders = headers}

-- | Set the HTTP method to use.
setMethod :: HT.StdMethod -> Request -> Request
setMethod m req = req{method = HT.renderStdMethod m}

defaultRequestHeaders :: [(HT.HeaderName, BS.ByteString)]
defaultRequestHeaders =
  [ (HT.hUserAgent, "hoauth2-2.8.0")
  , (HT.hAccept, "application/json")
  ]

-- | Create 'QueryParams' with given access token value.
accessTokenToParam :: AccessToken -> [(BS.ByteString, BS.ByteString)]
accessTokenToParam t = [("access_token", T.encodeUtf8 $ atoken t)]

-- | For `GET` method API.
appendAccessToken ::
  -- | Base URI
  URIRef a ->
  -- | Authorized Access Token
  AccessToken ->
  -- | Combined Result
  URIRef a
appendAccessToken uri t = over (queryL . queryPairsL) (\query -> query ++ accessTokenToParam t) uri

authGetJSONWithAuthMethod ::
  (MonadIO m, FromJSON a) =>
  APIAuthenticationMethod ->
  -- | HTTP connection manager.
  Manager ->
  AccessToken ->
  URI ->
  -- | Response as JSON
  m (StravaResponse (Either BSL.ByteString a))
authGetJSONWithAuthMethod authTypes manager t uri = do
  StravaResponse rlInfo resp <- authGetBSWithAuthMethod authTypes manager t uri
  pure $
    StravaResponse rlInfo $
      case resp of
        Left err -> Left err
        Right content -> either (Left . BSL.pack) Right $ eitherDecode content

data RateLimitInfo = RateLimitInfo
  { rateLimitDay :: Int
  , rateLimitFifteenMinutes :: Int
  , readRateLimitDay :: Int
  , readRateLimitFifteenMinutes :: Int
  , rateLimitUsedDay :: Int
  , rateLimitUsedFifteenMinutes :: Int
  , readRateLimitUsedDay :: Int
  , readRateLimitUsedFifteenMinutes :: Int
  }
  deriving (Show)

data StravaResponse a = StravaResponse
  { rateLimitInfo :: Maybe RateLimitInfo
  , response :: a
  }
  deriving (Show, Functor)

withRateLimitInfo :: Response LBS.ByteString -> StravaResponse LBS.ByteString
withRateLimitInfo resp =
  let rlInfo = do
        (rateLimitFifteenMinutes, rateLimitDay) <- extraxtTwoInts $ getResponseHeader "x-ratelimit-limit" resp
        (rateLimitUsedFifteenMinutes, rateLimitUsedDay) <- extraxtTwoInts $ getResponseHeader "x-ratelimit-usage" resp
        (readRateLimitFifteenMinutes, readRateLimitDay) <- extraxtTwoInts $ getResponseHeader "x-readratelimit-limit" resp
        (readRateLimitUsedFifteenMinutes, readRateLimitUsedDay) <- extraxtTwoInts $ getResponseHeader "x-readratelimit-usage" resp
        pure $
          RateLimitInfo
            { rateLimitFifteenMinutes = rateLimitFifteenMinutes
            , rateLimitDay = rateLimitDay
            , rateLimitUsedFifteenMinutes = rateLimitUsedFifteenMinutes
            , rateLimitUsedDay = rateLimitUsedDay
            , readRateLimitFifteenMinutes = readRateLimitFifteenMinutes
            , readRateLimitDay = readRateLimitDay
            , readRateLimitUsedFifteenMinutes = readRateLimitUsedFifteenMinutes
            , readRateLimitUsedDay = readRateLimitUsedDay
            }
   in StravaResponse
        { rateLimitInfo = rlInfo
        , response = getResponseBody resp
        }

safeRead :: (Read a) => String -> Maybe a
safeRead s =
  case reads s of
    [(x, "")] -> Just x
    _ -> Nothing

extraxtTwoInts :: [B.ByteString] -> Maybe (Int, Int)
extraxtTwoInts = \case
  [limits] ->
    case splitOn "," $ Char8.unpack limits of
      [rateLimitFifteenMinutes, rateLimitDay] -> do
        daily <- safeRead rateLimitDay
        fifteen <- safeRead rateLimitFifteenMinutes
        pure (fifteen, daily)
      _ -> Nothing
  _ -> Nothing

-- | Send an HTTP request.
authRequest ::
  (MonadIO m) =>
  -- | Request to perform
  Request ->
  -- | Modify request before sending
  (Request -> Request) ->
  -- | HTTP connection manager.
  Manager ->
  m (StravaResponse (Either BSL.ByteString BSL.ByteString))
authRequest req upReq manage = handleResponse <$> httpLbs (upReq req) manage

-- | Get response body out of a @Response@
handleResponse :: Response BSL.ByteString -> StravaResponse (Either BSL.ByteString BSL.ByteString)
handleResponse rsp
  | HT.statusIsSuccessful (responseStatus rsp) = Right <$> withRateLimitInfo rsp
  -- FIXME: better to surface up entire resp so that client can decide what to do when error happens.
  -- e.g. when 404, the response body could be empty hence library user has no idea what's happening.
  -- Which will be breaking changes.
  -- The current work around is surface up entire response as string.
  | BSL.null (responseBody rsp) = Left <$> (withRateLimitInfo rsp){response = BSL.pack $ show rsp}
  | otherwise = Left <$> withRateLimitInfo rsp

-- | Conduct POST request.
authPutBS ::
  -- | HTTP connection manager.
  Manager ->
  AccessToken ->
  URI ->
  PostBody ->
  -- | Response as ByteString
  IO (StravaResponse (Either BSL.ByteString BSL.ByteString))
authPutBS manager token url pb = do
  req <- uriToRequest url
  authRequest req upReq manager
  where
    upBody = urlEncodedBody (pb ++ [("access_token", encodeUtf8 $ atoken token)])
    upHeaders = updateRequestHeaders (Just token) . setMethod HT.PUT
    upReq = upHeaders . upBody

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

module Strava.API (
  Strava,
  getJSON,
  getBS,
  putJSON,
  runStrava,
  runStravaRateLimited,
  -- apiGetJSON,
  -- apiGetBS,
  -- apiPutJSON,
  Param (..),
  makeAPIURI,
  PathParam (..),
  Body (..),
  -- reexports
  UTCTime,
  Manager,
  OAuth2Token,
  StravaToken,
  requestAccessToken,
  Strava.API.refreshAccessToken,
  runStravaToken,
) where

import Control.Monad
import Control.Monad.Except
import Data.Aeson hiding (Error)
import Data.Bifunctor
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as Char8
import qualified Data.ByteString.Lazy.Char8 as LChar8
import Data.Foldable
import Data.Maybe
import qualified Data.Text as T
import Data.Time
import qualified Effects.Delay as Delay
import qualified Effects.Logging as Log
import Effects.Time
import Lens.Micro
import Network.HTTP.Client hiding (Request)
import Network.OAuth.OAuth2 (
  APIAuthenticationMethod (AuthInRequestHeader),
  ExchangeToken (ExchangeToken),
  OAuth2 (OAuth2),
  OAuth2Token (accessToken, refreshToken),
  TokenResponseError,
  authGetBS2,
  fetchAccessToken2,
  refreshAccessToken2,
 )
import Polysemy
import qualified Polysemy.Error as Error
import qualified Polysemy.State as State
import Strava.OAuth
import Strava.Types
import URI.ByteString
import URI.ByteString.QQ

apiBaseURL :: URI
apiBaseURL = [uri|https://www.strava.com/api/v3|]

tokenRequestURI :: URI
tokenRequestURI = [uri|https://www.strava.com/oauth/token|]

tokenRedirectURI :: URI
tokenRedirectURI = [uri|https://www.strava.com/oauth/token|]

class Param p where
  asParam :: p -> B.ByteString -> Maybe (B.ByteString, B.ByteString)

instance (Param p) => Param (Maybe p) where
  asParam mp = (>>=) mp . flip asParam

instance Param Int where
  asParam p name = Just (name, Char8.toStrict $ LChar8.pack $ show p)

instance Param UTCTime where
  asParam p name = Just (name, Char8.toStrict $ LChar8.pack $ formatTime defaultTimeLocale "%s" p)

instance Param Bool where
  asParam p name = Just (name, Char8.toStrict $ LChar8.pack $ show p)

instance (Param a) => Param [a] where
  asParam ps name = pure $ (,) name $ B.intercalate "," $ mapMaybe (fmap snd . (`asParam` name)) ps

class Body b where
  asBody :: b -> [(B.ByteString, B.ByteString)]

class PathParam p where
  asPathParam :: p -> B.ByteString

instance PathParam String where
  asPathParam = Char8.toStrict . LChar8.pack

makeAPIURI :: B.ByteString -> [Maybe (B.ByteString, B.ByteString)] -> URI
makeAPIURI path params =
  apiBaseURL
    & pathL
    <>~ path
    & queryL
    . queryPairsL
    .~ catMaybes params

data Strava m a where
  GetJSONInternal :: (FromJSON a) => URI -> Strava m (StravaResponse a)
  GetBS :: URI -> Strava m LChar8.ByteString
  PutJSONInternal :: (Body reqBody, FromJSON a) => URI -> reqBody -> Strava m (StravaResponse a)

makeSem ''Strava

getJSON ::
  (Member Strava r, FromJSON a) =>
  URI ->
  Sem r a
getJSON = fmap response . getJSONInternal

-- getBS ::
--   (Member Strava r) =>
--   URI ->
--   Sem r LChar8.ByteString
-- getBS = fmap response . getBSInternal

putJSON ::
  (Member Strava r, FromJSON a, Body reqBody) =>
  URI ->
  reqBody ->
  Sem r a
putJSON uri = fmap response . putJSONInternal uri

runStrava :: (Members '[Embed IO, Error.Error Fault, Error.Error ResourceNotFound, Log.Log] r) => Manager -> OAuth2Token -> Sem (Strava : r) a -> Sem r a
runStrava manager oauth =
  interpret $ \case
    GetJSONInternal uri -> do
      StravaResponse rlInfo resp <- authGetJSONWithAuthMethod AuthInRequestHeader manager (accessToken oauth) uri
      Log.logDebug $ "RateLimitInfo: " <> show rlInfo
      stravaErrors $ StravaResponse rlInfo <$> first decodeFault resp
    GetBS uri -> stravaErrorsM $ decodeResponse <$> runExceptT (authGetBS2 manager (accessToken oauth) uri)
    PutJSONInternal uri reqBody -> do
      StravaResponse rlInfo resp <- embed $ authPutBS manager (accessToken oauth) uri $ asBody reqBody
      Log.logDebug $ "RateLimitInfo: " <> show rlInfo
      stravaErrors $ StravaResponse rlInfo <$> either (Left . decodeFault) (decodeResponse . first LChar8.pack . eitherDecode) resp

stravaErrorsM :: (Members '[Embed m, Error.Error Fault, Error.Error ResourceNotFound] r) => m (Either (Either Fault ResourceNotFound) a) -> Sem r a
stravaErrorsM = embed >=> stravaErrors

stravaErrors :: (Members '[Error.Error Fault, Error.Error ResourceNotFound] r) => Either (Either Fault ResourceNotFound) a -> Sem r a
stravaErrors = \case
  Right x -> pure x
  Left (Left fault) -> Error.throw fault
  Left (Right ResourceNotFound) -> Error.throw ResourceNotFound

startOfNextDay :: UTCTime -> UTCTime
startOfNextDay (UTCTime day _) = UTCTime (addDays 1 day) 0

startOfNextQuarterHour :: UTCTime -> UTCTime
startOfNextQuarterHour (UTCTime day time)
  | time > 86400 - 15 * 60 =
      -- last quarter hour of day
      UTCTime (addDays 1 day) 0
  | otherwise =
      UTCTime day (fromIntegral $ ((round @_ @Int time `div` 900) + 1) * 900)

requestDelay :: (Members '[State.State RateLimitInfo, Time, Log.Log] r) => Bool -> Sem r (Maybe NominalDiffTime)
requestDelay isReadRequest | isReadRequest = do
  rlInfo <- State.get
  Log.logDebug $ "Current " <> show rlInfo
  if
    | rlInfo.readRateLimitUsedFifteenMinutes < rlInfo.readRateLimitFifteenMinutes
        && rlInfo.readRateLimitUsedDay < rlInfo.readRateLimitDay ->
        pure Nothing
    | rlInfo.readRateLimitUsedDay < rlInfo.readRateLimitDay -> do
        Log.logDebug "wait till next quater hour"
        now <- getTime
        let nextValidRequestTime = startOfNextQuarterHour now
        pure $ Just $ diffUTCTime nextValidRequestTime now
    | otherwise -> do
        Log.logDebug "wait till next day"
        now <- getTime
        let nextValidRequestTime = startOfNextDay now
        pure $ Just $ diffUTCTime nextValidRequestTime now
requestDelay _ = do
  rlInfo <- State.get
  if
    | rlInfo.rateLimitUsedFifteenMinutes < rlInfo.rateLimitFifteenMinutes
        && rlInfo.rateLimitUsedDay < rlInfo.rateLimitDay ->
        pure Nothing
    | rlInfo.rateLimitUsedDay < rlInfo.rateLimitDay -> do
        Log.logDebug "wait till next quater hour"
        now <- getTime
        let nextValidRequestTime = startOfNextQuarterHour now
        pure $ Just $ diffUTCTime nextValidRequestTime now
    | otherwise -> do
        Log.logDebug "wait till next day"
        now <- getTime
        let nextValidRequestTime = startOfNextDay now
        pure $ Just $ diffUTCTime nextValidRequestTime now

updateRateLimitInfo :: (Members '[State.State RateLimitInfo, Log.Log] r) => Maybe RateLimitInfo -> Sem r ()
updateRateLimitInfo rlInfo = do
  Log.logDebug $ "New RateLimitInfo: " <> show rlInfo
  for_ rlInfo State.put

runStravaRateLimited ::
  ( Members
      '[ Embed IO
       , Error.Error Fault
       , Error.Error ResourceNotFound
       , Log.Log
       , State.State RateLimitInfo
       , Delay.Delay
       , Time
       ]
      r
  ) =>
  Manager ->
  OAuth2Token ->
  Sem (Strava : r) a ->
  Sem r a
runStravaRateLimited manager oauth =
  interpret $ \case
    GetJSONInternal uri -> do
      potentialRequestDelay <- requestDelay True
      case potentialRequestDelay of
        Nothing -> pure ()
        Just requestDelay -> do
          Log.logDebug $ "Rate limiting delay: " <> show requestDelay
          now <- getTime
          Log.logDebug $ "Resuming: " <> show (addUTCTime requestDelay now)
          Delay.delay requestDelay
      StravaResponse rlInfo resp <- authGetJSONWithAuthMethod AuthInRequestHeader manager (accessToken oauth) uri
      Log.logDebug $ "RateLimitInfo: " <> show rlInfo
      updateRateLimitInfo rlInfo
      stravaErrors $ first (first (specifyFaultLocation ("GetJSONInternal " <> show uri))) $ StravaResponse rlInfo <$> first decodeFault resp
    GetBS uri ->
      stravaErrorsM $
        first (first (specifyFaultLocation ("GetBS " <> show uri))) . decodeResponse
          <$> runExceptT (authGetBS2 manager (accessToken oauth) uri)
    -- GetBSInternal uri -> do
    --   potentialRequestDelay <- requestDelay True
    --   Log.logDebug $ "Rate limiting delay: " <> show potentialRequestDelay
    --   for_ potentialRequestDelay Delay.delay

    --   resp@(StravaResponse rlInfo _) <- getBSInternal uri
    --   updateRateLimitInfo rlInfo
    --   pure resp
    --  Error.fromEitherM $ decodeResponse <$> runExceptT (authGetBS2 manager (accessToken oauth) uri)
    PutJSONInternal uri reqBody -> do
      potentialRequestDelay <- requestDelay False
      Log.logDebug $ "Rate limiting delay: " <> show potentialRequestDelay
      for_ potentialRequestDelay Delay.delay
      StravaResponse rlInfo resp <- embed $ authPutBS manager (accessToken oauth) uri $ asBody reqBody
      Log.logDebug $ "RateLimitInfo: " <> show rlInfo
      updateRateLimitInfo rlInfo
      stravaErrors $
        first (first (specifyFaultLocation ("PutJSONInternal " <> show uri <> " reqBody"))) $
          StravaResponse rlInfo <$> either (Left . decodeFault) (decodeResponse . first LChar8.pack . eitherDecode) resp

decodeResponse :: Either LChar8.ByteString a -> Either (Either Fault ResourceNotFound) a
decodeResponse (Right x) = Right x
decodeResponse (Left err) = Left $ decodeFault err

decodeFault :: LChar8.ByteString -> Either Fault ResourceNotFound
decodeFault err = case eitherDecode err of
  Left err' -> Left $ Fault [Error (show err) "" ""] err'
  Right fault
    | fault.faultMessage == "Record Not Found" -> Right ResourceNotFound
    | fault.faultMessage == "Resource Not Found" -> Right ResourceNotFound
    | otherwise -> Left fault

data StravaToken m a where
  RequestAccessToken :: String -> StravaToken m OAuth2Token
  RefreshAccessToken :: OAuth2Token -> StravaToken m OAuth2Token

makeSem ''StravaToken

runStravaToken :: (Members '[Embed IO, Error.Error TokenResponseError] r) => String -> String -> Manager -> Sem (StravaToken : r) a -> Sem r a
runStravaToken clientId clientSecret manager = interpret $ \case
  RequestAccessToken startTok -> do
    let exchangeToken = ExchangeToken $ T.pack startTok
    let oauth = OAuth2 (T.pack clientId) (T.pack clientSecret) tokenRequestURI tokenRequestURI tokenRedirectURI
    Error.fromEitherM $ runExceptT $ fetchAccessToken2 manager oauth exchangeToken
  RefreshAccessToken tok -> do
    let oauth = OAuth2 (T.pack clientId) (T.pack clientSecret) tokenRequestURI tokenRequestURI tokenRedirectURI
    Error.fromEitherM $
      runExceptT $ case refreshToken tok of
        Just refreshTok -> refreshAccessToken2 manager oauth refreshTok
        Nothing -> error "Could not refresh, no refresh token given"

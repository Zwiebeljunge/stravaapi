{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE UndecidableInstances #-}

module Strava.APITypes where

import Control.Lens
import Data.Aeson
import Data.Aeson.Lens
import Data.Aeson.Types
import qualified Data.ByteString.Lazy.Char8 as Char8
import Data.Foldable
import Data.Maybe
import Data.String
import qualified Data.Text as T
import Data.Time (NominalDiffTime)
import GHC.Generics (Generic, Rep)
import Strava.API

genericStravaAPIJSON :: (Generic a, GFromJSON Zero (Rep a)) => Int -> Value -> Parser a
genericStravaAPIJSON n =
  genericParseJSON (defaultOpts n)

defaultOpts :: Int -> Options
defaultOpts n =
  defaultOptions
    { fieldLabelModifier = replaceLatLng . camelTo2 '_' . drop n
    }
  where
    replaceLatLng = T.unpack . T.replace "lat_lng" "latlng" . T.pack

genericStravaAPIToJSON :: (Generic a, GToJSON' Value Zero (Rep a)) => Int -> a -> Value
genericStravaAPIToJSON n =
  genericToJSON (defaultOpts n)

genericResourceDependent ::
  (FromJSON summary, FromJSON detailed) =>
  String ->
  (meta -> Maybe summary -> Maybe detailed -> complete) ->
  (Object -> Parser meta) ->
  Value ->
  Parser complete
genericResourceDependent name constructor p1 =
  parseJSONResourceDependent name constructor p1 parseJSON parseJSON

parseJSONResourceDependent ::
  String ->
  (meta -> Maybe summary -> Maybe detailed -> complete) ->
  (Object -> Parser meta) ->
  (Value -> Parser summary) ->
  (Value -> Parser detailed) ->
  Value ->
  Parser complete
parseJSONResourceDependent name constructor p1 p2 p3 v =
  withObject
    name
    ( \o ->
        uncurry . constructor
          <$> p1 o
          <*> ( o .: "resource_state" >>= \case
                  Meta -> pure (Nothing, Nothing)
                  Summary -> (,Nothing) . Just <$> p2 v
                  Detailed -> (,) . Just <$> p2 v <*> (Just <$> p3 v)
              )
    )
    v

data ResourceState = Meta | Summary | Detailed
  deriving (Show)

instance FromJSON ResourceState where
  parseJSON = withScientific "ResourceState" $ \case
    1 -> pure Meta
    2 -> pure Summary
    3 -> pure Detailed
    _ -> mempty

data Sex = Male | Female
  deriving (Show)

instance FromJSON Sex where
  parseJSON = withText "Sex" $ \case
    "M" -> pure Male
    "F" -> pure Female
    _ -> mempty

data Unit = Feet | Meter
  deriving (Show)

instance FromJSON Unit where
  parseJSON = withText "Unit" $ \case
    "feet" -> pure Feet
    "meters" -> pure Meter
    _ -> mempty

data ActivityStats = ActivityStats
  { activityStatsBiggestRideDistance :: Double
  , activityStatsBiggestClimbElevationGain :: Double
  , activityStatsRecentRideTotals :: ActivityTotal
  , activityStatsRecentRunTotals :: ActivityTotal
  , activityStatsRecentSwimTotals :: ActivityTotal
  , activityStatsYTDRideTotals :: ActivityTotal
  , activityStatsYTDRunTotals :: ActivityTotal
  , activityStatsYTDSwimTotals :: ActivityTotal
  , activityStatsAllRideTotals :: ActivityTotal
  , activityStatsAllRunTotals :: ActivityTotal
  , activityStatsAllSwimTotals :: ActivityTotal
  }
  deriving (Show, Generic)

instance FromJSON ActivityStats where
  parseJSON = genericStravaAPIJSON 13

data ActivityTotal = ActivityTotal
  { activityTotalCount :: Integer
  , activityTotalDistance :: Double
  , activityTotalMovingTime :: NominalDiffTime
  , activityTotalElapsedTime :: NominalDiffTime
  , activityTotalElevationGain :: Double
  , activityTotalAchievementCount :: Maybe Int
  }
  deriving (Show, Generic)

instance FromJSON ActivityTotal where
  parseJSON = genericStravaAPIJSON 13

data ActivityType
  = AlpineSki
  | BackcountrySki
  | Canoeing
  | Crossfit
  | EBikeRide
  | Elliptical
  | Golf
  | Handcycle
  | Hike
  | IceSkate
  | InlineSkate
  | Kayaking
  | Kitesurf
  | NordicSki
  | Ride
  | RockClimbing
  | RollerSki
  | Rowing
  | Run
  | Sail
  | Skateboard
  | Snowboard
  | Snowshoe
  | Soccer
  | StairStepper
  | StandUpPaddling
  | Surfing
  | Swim
  | Velomobile
  | VirtualRide
  | VirtualRun
  | Walk
  | WeightTraining
  | Wheelchair
  | Windsurf
  | Workout
  | Yoga
  deriving (Show, Eq)

instance FromJSON ActivityType where
  parseJSON = withText "ActivityType" $ \case
    "AlpineSki" -> pure AlpineSki
    "BackcountrySki" -> pure BackcountrySki
    "Canoeing" -> pure Canoeing
    "Crossfit" -> pure Crossfit
    "EBikeRide" -> pure EBikeRide
    "Elliptical" -> pure Elliptical
    "Golf" -> pure Golf
    "Handcycle" -> pure Handcycle
    "Hike" -> pure Hike
    "IceSkate" -> pure IceSkate
    "InlineSkate" -> pure InlineSkate
    "Kayaking" -> pure Kayaking
    "Kitesurf" -> pure Kitesurf
    "NordicSki" -> pure NordicSki
    "Ride" -> pure Ride
    "RockClimbing" -> pure RockClimbing
    "RollerSki" -> pure RollerSki
    "Rowing" -> pure Rowing
    "Run" -> pure Run
    "Sail" -> pure Sail
    "Skateboard" -> pure Skateboard
    "Snowboard" -> pure Snowboard
    "Snowshoe" -> pure Snowshoe
    "Soccer" -> pure Soccer
    "StairStepper" -> pure StairStepper
    "StandUpPaddling" -> pure StandUpPaddling
    "Surfing" -> pure Surfing
    "Swim" -> pure Swim
    "Velomobile" -> pure Velomobile
    "VirtualRide" -> pure VirtualRide
    "VirtualRun" -> pure VirtualRun
    "Walk" -> pure Walk
    "WeightTraining" -> pure WeightTraining
    "Wheelchair" -> pure Wheelchair
    "Windsurf" -> pure Windsurf
    "Workout" -> pure Workout
    "Yoga" -> pure Yoga
    _ -> mempty

instance ToJSON ActivityType where
  toJSON = String . fromString . show

data ActivityZone = ActivityZone
  { activityZoneScore :: Maybe Integer
  , activityZoneDistributionBuckets :: TimedZoneDistribution
  , activityZoneType :: ActivityZoneType
  , activityZoneSensorBased :: Bool
  , activityZonePoints :: Maybe Integer
  , activityZoneCustomZones :: Maybe Bool
  , activityZoneMax :: Maybe Integer
  }
  deriving (Show, Generic)

instance FromJSON ActivityZone where
  parseJSON = genericStravaAPIJSON 12

data ActivityZoneType = Heartrate | Power
  deriving (Show)

instance FromJSON ActivityZoneType where
  parseJSON = withText "ActivityZoneType" $ \case
    "heartrate" -> pure Heartrate
    "power" -> pure Power
    _ -> mempty

data BaseStream = BaseStream
  { baseStreamOriginalSize :: Integer
  , baseStreamResolution :: StreamResolution
  , baseStreamSeriesType :: SeriesType
  }
  deriving (Show)

data StreamResolution = Low | Medium | High
  deriving (Show)

instance FromJSON StreamResolution where
  parseJSON (String "low") = pure Low
  parseJSON (String "medium") = pure Medium
  parseJSON (String "high") = pure High
  parseJSON v = typeMismatch "StreamResolution" v

data SeriesType = Distance | Time
  deriving (Show)

instance FromJSON SeriesType where
  parseJSON (String "distance") = pure Distance
  parseJSON (String "time") = pure Time
  parseJSON v = typeMismatch "SeriesType" v

newtype ClubId = ClubId Integer
  deriving (Show)

instance FromJSON ClubId where
  parseJSON v = ClubId <$> parseJSON v

instance PathParam ClubId where
  asPathParam (ClubId cId) = asPathParam $ show cId

newtype RouteId = RouteId Integer
  deriving (Show)

instance FromJSON RouteId where
  parseJSON v = RouteId <$> parseJSON v

instance PathParam RouteId where
  asPathParam (RouteId rId) = asPathParam $ show rId

newtype ActivityId = ActivityId Integer
  deriving (Show, Eq, Ord, Generic)

instance FromJSON ActivityId where
  parseJSON v = ActivityId <$> parseJSON v

instance PathParam ActivityId where
  asPathParam (ActivityId actId) = asPathParam $ show actId

newtype SegmentId = SegmentId Integer
  deriving (Show)

instance FromJSON SegmentId where
  parseJSON v = SegmentId <$> parseJSON v

instance PathParam SegmentId where
  asPathParam (SegmentId segId) = asPathParam $ show segId

instance Param SegmentId where
  asParam (SegmentId segId) paramName = Just (paramName, Char8.toStrict $ Char8.pack $ show segId)

newtype AthleteId = AthleteId Integer
  deriving (Show)

instance FromJSON AthleteId where
  parseJSON v = AthleteId <$> parseJSON v

instance PathParam AthleteId where
  asPathParam (AthleteId aId) = asPathParam $ show aId

newtype CommentId = CommentId Integer
  deriving (Show)

instance FromJSON CommentId where
  parseJSON v = CommentId <$> parseJSON v

data Comment = Comment
  { commentId :: CommentId
  , commentActivityId :: ActivityId
  , commentText :: String
  , commentAthlete :: Athlete
  , commentCreatedAt :: UTCTime
  }
  deriving (Show, Generic)

instance FromJSON Comment where
  parseJSON = genericStravaAPIJSON 7

newtype ExplorerResponse = ExplorerResponse
  { explorerResponseSegments :: [ExplorerSegment]
  }
  deriving (Show)

data ExplorerSegment = ExplorerSegment
  { explorerSegmentId :: SegmentId
  , explorerSegmentName :: String
  , explorerSegmentClimbCategory :: ClimbCategory
  , explorerSegmentClimbCategoryDescription :: ClimbCategoryDescription
  , explorerSegmentAvgGrade :: Double
  , explorerSegmentStartLatLng :: LatLng
  , explorerSegmentEndLatLng :: LatLng
  , explorerSegmentElevDifference :: Double
  , explorerSegmentDistance :: Double
  , explorerSegmentPoints :: String
  }
  deriving (Show)

data ClimbCategory = CCOne | CCTwo | CCThree | CCFour | CCFive
  deriving (Show)

-- instance FromJSON ClimbCategory where
--   parseJSON = withScientific "ClimbCategory" $ \s ->
--                 case s of
--                   1

data ClimbCategoryDescription = NC | Four | Three | Two | One | HC
  deriving (Show)

data HeartRateZoneRanges = HeartRateZoneRanges
  { heartRateZoneRangesCustomZones :: Bool
  , heartRateZoneRangesZones :: ZoneRanges
  }
  deriving (Show)

newtype LapId = LapId Integer
  deriving (Show)

instance FromJSON LapId where
  parseJSON v = LapId <$> parseJSON v

newtype Lap = Lap
  { lapId :: LapId
  }
  deriving (Show, Generic)

instance FromJSON Lap where
  parseJSON = genericStravaAPIJSON 3

newtype LatLng = LatLng (Double, Double)
  deriving (Show)

instance FromJSON LatLng where
  parseJSON v = LatLng <$> parseJSON v

data PhotosSummary = PhotosSummary
  deriving (Show)

instance FromJSON PhotosSummary where
  parseJSON _ = pure PhotosSummary

data PhotosSummaryPrimary

data PolylineMap = PolylineMap
  deriving (Show)

instance FromJSON PolylineMap where
  parseJSON _ = pure PolylineMap

data PowerZoneRanges

data Route = Route
  { routeAthlete :: Athlete
  , routeDescription :: String
  , routeDistance :: Double
  , routeElevationGain :: Double
  , routeId :: RouteId
  , routeIdStr :: String
  , routeMap :: PolylineMap
  , routeName :: String
  , routePrivate :: Bool
  , routeStarred :: Bool
  , routeTimestamp :: Integer
  , routeType :: RouteType
  , routeSubType :: RouteSubType
  , routeCreatedAt :: UTCTime
  , routeUpdatedAt :: UTCTime
  , routeEstimatedMovingTime :: Int
  , routeSegments :: Maybe [SummarySegment]
  }
  deriving (Show, Generic)

instance FromJSON Route where
  parseJSON = genericStravaAPIJSON 5

data RouteType = RouteTypeRide | RouteTypeRun
  deriving (Show)

instance FromJSON RouteType where
  parseJSON = withScientific "RouteType" $ \case
    1 -> pure RouteTypeRide
    2 -> pure RouteTypeRun
    _ -> mempty

data RouteSubType = Road | MountainBike | Cross | Trail | Mixed
  deriving (Show)

instance FromJSON RouteSubType where
  parseJSON = withScientific "RouteSubType" $ \case
    1 -> pure Road
    2 -> pure MountainBike
    3 -> pure Cross
    4 -> pure Trail
    5 -> pure Mixed
    _ -> mempty

data Split = Split
  { splitAverageSpeed :: Double
  , splitDistance :: Double
  , splitElapsedTime :: NominalDiffTime
  , splitElevationDifference :: Double
  , splitPaceZone :: Integer
  , splitMovingTime :: NominalDiffTime
  , splitSplit :: Integer
  }
  deriving (Show, Generic)

instance FromJSON Split where
  parseJSON = genericStravaAPIJSON 5

data StreamSet = StreamSet
  { timeStream :: Maybe (Stream Double)
  , distanceStream :: Maybe (Stream Double)
  , latlngStream :: Maybe (Stream (Double, Double))
  , altituteStream :: Maybe (Stream Double)
  , velocitySmoothStream :: Maybe (Stream Double)
  , heartrateStream :: Maybe (Stream Double)
  , cadenceStream :: Maybe (Stream Double)
  , wattsStream :: Maybe (Stream Double)
  , tempStream :: Maybe (Stream Double)
  , movingStream :: Maybe (Stream Bool)
  , gradeSmoothStream :: Maybe (Stream Double)
  }

instance FromJSON StreamSet where
  parseJSON =
    withArray
      "StreamSet"
      ( \streams ->
          let correctStream wantedStream =
                find
                  ( \v ->
                      case v ^? key "type" . _JSON of
                        Just x | x == wantedStream -> True
                        _ -> False
                  )
                  streams
           in StreamSet
                <$> traverse parseJSON (correctStream TimeStream)
                <*> traverse parseJSON (correctStream DistanceStream)
                <*> traverse parseJSON (correctStream LatLngStream)
                <*> traverse parseJSON (correctStream AltitudeStream)
                <*> traverse parseJSON (correctStream VelocitySmoothStream)
                <*> traverse parseJSON (correctStream HeartrateStream)
                <*> traverse parseJSON (correctStream CadenceStream)
                <*> traverse parseJSON (correctStream WattsStream)
                <*> traverse parseJSON (correctStream TempStream)
                <*> traverse parseJSON (correctStream MovingStream)
                <*> traverse parseJSON (correctStream GradeSmoothStream)
      )

data SummaryGear = SummaryGear
  { summaryGearPrimary :: Bool
  , summaryGearName :: String
  , summaryGearDistance :: Double
  }
  deriving (Show, Generic)

instance FromJSON SummaryGear where
  parseJSON = genericStravaAPIJSON 11

data SummarySegment = SummarySegment
  { summarySegmentId :: SegmentId
  , summarySegmentName :: String
  , summarySegmentActivityType :: ActivityType
  , summarySegmentDistance :: Double
  , summarySegmentAverageGrade :: Double
  , summarySegmentMaximumGrade :: Double
  , summarySegmentElevationHigh :: Double
  , summarySegmentElevationLow :: Double
  , summarySegmentStartLatLng :: LatLng
  , summarySegmentEndLatLng :: LatLng
  , -- , summarySegmentClimbCategory :: ClimbCategory
    summarySegmentCity :: Maybe String
  , summarySegmentState :: Maybe String
  , summarySegmentCountry :: Maybe String
  }
  deriving (Show, Generic)

instance FromJSON SummarySegment where
  parseJSON = genericStravaAPIJSON 14

newtype TimedZoneDistribution = TimedZoneDistribution [TimedZoneRange]
  deriving (Show)

instance FromJSON TimedZoneDistribution where
  parseJSON = fmap TimedZoneDistribution . parseJSON

data UpdateableActivity = UpdateableActivity
  { updateableActivityCommute :: Bool
  , updateableActivityTrainer :: Bool
  , updateableActivityHideFromHome :: Bool
  , updateableActivityDescription :: Maybe String
  , updateableActivityName :: Maybe String
  , updateableActivityType :: Maybe ActivityType
  , updateableActivityGearId :: UpdateableGearId
  }
  deriving (Show, Generic)

instance Body UpdateableActivity where
  asBody uAct =
    [ ("commute", showBool $ updateableActivityCommute uAct)
    , ("trainer", showBool $ updateableActivityTrainer uAct)
    , ("hide_from_home", showBool $ updateableActivityHideFromHome uAct)
    , ("gear_id", updateableGearIdToByteString $ updateableActivityGearId uAct)
    ]
      <> case updateableActivityName uAct of
        Nothing -> []
        Just name ->
          [("name", fromString name)]
      <> case updateableActivityType uAct of
        Nothing -> []
        Just actType ->
          [("type", fromString $ show actType)]
      <> case updateableActivityDescription uAct of
        Nothing -> []
        Just desc ->
          [("description", fromString desc)]
    where
      showBool True = "true"
      showBool False = "false"

      updateableGearIdToByteString None = "none"
      updateableGearIdToByteString (NewGearId gId) = fromString gId

data UpdateableGearId
  = None
  | NewGearId String
  deriving (Show)

data Upload = Upload
  { uploadId :: UploadId
  , uploadIdStr :: String
  , uploadExternalId :: String
  , uploadError :: String
  , uploadStatus :: String
  , uploadActivityId :: ActivityId
  }
  deriving (Show, Generic)

instance FromJSON Upload where
  parseJSON = genericStravaAPIJSON 6

data ZoneRange = ZoneRange
  { zoneRangeMin :: Int
  , zoneRangeMax :: Int
  }
  deriving (Show, Generic)

instance FromJSON ZoneRange where
  parseJSON = genericStravaAPIJSON 9

data ZoneRanges = ZoneRanges
  { zoneRangesZones :: [ZoneRange]
  , zoneRangesCustomZones :: Maybe Bool
  }
  deriving (Show, Generic)

instance FromJSON ZoneRanges where
  parseJSON = genericStravaAPIJSON 10

data Zones = Zones
  { zonesHeartRate :: Maybe ZoneRanges
  , zonesPower :: Maybe ZoneRanges
  }
  deriving (Show, Generic)

instance FromJSON Zones where
  parseJSON = genericStravaAPIJSON 5

data StreamType
  = TimeStream
  | DistanceStream
  | LatLngStream
  | AltitudeStream
  | VelocitySmoothStream
  | HeartrateStream
  | CadenceStream
  | WattsStream
  | TempStream
  | MovingStream
  | GradeSmoothStream
  deriving (Show, Enum, Eq)

instance FromJSON StreamType where
  parseJSON (String "time") = pure TimeStream
  parseJSON (String "distance") = pure DistanceStream
  parseJSON (String "latlng") = pure LatLngStream
  parseJSON (String "altitude") = pure AltitudeStream
  parseJSON (String "velocity_smooth") = pure VelocitySmoothStream
  parseJSON (String "heartrate") = pure HeartrateStream
  parseJSON (String "cadence") = pure CadenceStream
  parseJSON (String "watts") = pure WattsStream
  parseJSON (String "temp") = pure TempStream
  parseJSON (String "moving") = pure MovingStream
  parseJSON (String "grade_smooth") = pure GradeSmoothStream
  parseJSON v = typeMismatch "StreamType" v

instance ToJSON StreamType where
  toJSON = error "this is just a stub"

instance Param StreamType where
  asParam st name =
    let v = case st of
          TimeStream -> "time"
          DistanceStream -> "distance"
          LatLngStream -> "latlng"
          AltitudeStream -> "altitude"
          VelocitySmoothStream -> "velocity_smooth"
          HeartrateStream -> "heartrate"
          CadenceStream -> "cadence"
          WattsStream -> "watts"
          TempStream -> "temp"
          MovingStream -> "moving"
          GradeSmoothStream -> "grade_smooth"
     in Just (name, v)

data Stream a = Stream
  { streamOriginalSize :: Integer
  , streamType :: StreamType
  , streamSeriesType :: SeriesType
  , streamResolution :: StreamResolution
  , streamData :: [a]
  }
  deriving (Show, Generic, Functor)

instance (FromJSON a) => FromJSON (Stream a) where
  parseJSON = genericStravaAPIJSON 6

data Gear = Gear
  { gearId :: String
  , gearSummary :: Maybe SummaryGear
  , gearDetailed :: Maybe DetailedGear
  }
  deriving (Show)

instance FromJSON Gear where
  parseJSON =
    genericResourceDependent
      "Gear"
      Gear
      (.: "id")

data DetailedGear = DetailedGear
  { detailedGearBrandName :: String
  , detailedGearModelName :: String
  , detailedGearFrameType :: Int
  , detailedGearDescription :: String
  }
  deriving (Show, Generic)

instance FromJSON DetailedGear where
  parseJSON = genericStravaAPIJSON 12

data DetailedSegment = DetailedSegment
  { detailedSegmentState :: Maybe String
  , detailedSegmentActivityType :: ActivityType
  , detailedSegmentPrivate :: Bool
  , detailedSegmentCountry :: Maybe String
  , detailedSegmentDistance :: Double
  , detailedSegmentElevationHigh :: Double
  , detailedSegmentEndLatLng :: LatLng
  , detailedSegmentElevationLow :: Double
  , detailedSegmentName :: String
  , detailedSegmentCity :: Maybe String
  , detailedSegmentStarred :: Bool
  , detailedSegmentMaximumGrade :: Double
  , detailedSegmentHazardous :: Bool
  , detailedSegmentId :: SegmentId
  , detailedSegmentStartLatLng :: LatLng
  , detailedSegmentClimbCategory :: Int
  , detailedSegmentAverageGrade :: Double
  }
  deriving (Show, Generic)

instance FromJSON DetailedSegment where
  parseJSON = genericStravaAPIJSON 15

newtype SegmentEffortId = SegmentEffortId Integer
  deriving (Show)

instance FromJSON SegmentEffortId where
  parseJSON v = SegmentEffortId <$> parseJSON v

data SegmentEffort = SegmentEffort
  { segmentEffortId :: SegmentEffortId
  , segmentEffortSummary :: Maybe SummarySegmentEffort
  , segmentEffortDetailed :: Maybe DetailedSegmentEffort
  }
  deriving (Show)

instance FromJSON SegmentEffort where
  parseJSON =
    genericResourceDependent
      "SegmentEffort"
      SegmentEffort
      (.: "id")

data SummarySegmentEffort = SummarySegmentEffort
  { summarySegmentEffortActivityId :: Maybe ActivityId
  , summarySegmentEffortElapsedTime :: Integer
  , summarySegmentEffortStartDate :: UTCTime
  , summarySegmentEffortStartDateLocal :: UTCTime
  , summarySegmentEffortDistance :: Double
  , summarySegmentEffortIsKom :: Maybe Bool
  }
  deriving (Show, Generic)

instance FromJSON SummarySegmentEffort where
  parseJSON = genericStravaAPIJSON 20

data DetailedSegmentEffort = DetailedSegmentEffort
  { detailedSegmentEffortName :: String
  , detailedSegmentEffortActivity :: Activity
  , detailedSegmentEffortAthlete :: Athlete
  , detailedSegmentEffortMovingTime :: Double
  , detailedSegmentEffortStartIndex :: Int
  , detailedSegmentEffortEndIndex :: Int
  , detailedSegmentEffortAverageCadence :: Maybe Double
  , detailedSegmentEffortAverageWatts :: Maybe Double
  , detailedSegmentEffortDeviceWatts :: Maybe Bool
  , detailedSegmentEffortAverageHeartrate :: Maybe Double
  , detailedSegmentEffortMaxHeartrate :: Maybe Double
  , detailedSegmentEffortSegment :: Maybe DetailedSegment
  , detailedSegmentEffortKomRank :: Maybe Int
  , detailedSegmentEffortPrRank :: Double
  , detailedSegmentEffortHidden :: Maybe Bool
  }
  deriving (Show, Generic)

instance FromJSON DetailedSegmentEffort where
  parseJSON = genericStravaAPIJSON 21

newtype UploadId = UploadId Integer
  deriving (Show)

instance FromJSON UploadId where
  parseJSON v = UploadId <$> parseJSON v

instance PathParam UploadId where
  asPathParam (UploadId uId) = asPathParam $ show uId

data SummaryActivity = SummaryActivity
  { summaryActivityExernalId :: Maybe String
  , summaryActivityUploadId :: Maybe UploadId
  , summaryActivityAthlete :: Maybe Athlete
  , summaryActivityName :: String
  , summaryActivityDistance :: Double
  , summaryActivityMovingTime :: NominalDiffTime
  , summaryActivityElapsedTime :: NominalDiffTime
  , summaryActivityTotalElevationGain :: Double
  , summaryActivityElevationHigh :: Maybe Double
  , summaryActivityElevationLow :: Maybe Double
  , summaryActivityType :: ActivityType
  , summaryActivityStartDate :: Maybe UTCTime
  , summaryActivityStartDateLocal :: Maybe UTCTime
  , summaryActivityTimezone :: Maybe String
  , detailedActivityStartLatLng :: Maybe LatLng
  , detailedActivityEndLatLng :: Maybe LatLng
  , summaryActivityAchievementCount :: Maybe Int
  , summaryActivityKudosCount :: Maybe Int
  , summaryActivityCommentCount :: Maybe Int
  , summaryActivityAthleteCount :: Maybe Int
  , summaryActivityPhotoCount :: Maybe Int
  , summaryActivityTotalPhotoCount :: Maybe Int
  , summaryActivityMap :: Maybe PolylineMap
  , summaryActivityTrainer :: Maybe Bool
  , summaryActivityCommute :: Maybe Bool
  , summaryActivityManual :: Maybe Bool
  , summaryActivityPrivate :: Maybe Bool
  , summaryActivityFlagged :: Maybe Bool
  , summaryActivityWorkoutType :: Maybe Int
  , summaryActivityUploadIdStr :: Maybe String
  , summaryActivityAverageSpeed :: Maybe Double
  , summaryActivityMaxSpeed :: Maybe Double
  , summaryActivityHasKudoed :: Maybe Bool
  , summaryActivityHideFromHome :: Maybe Bool
  , summaryActivityGearId :: Maybe String
  , summaryActivityKiloJoules :: Maybe Double
  , summaryActivityAverageWatts :: Maybe Double
  , summaryActivityDeviceWatts :: Maybe Bool
  , summaryActivityMaxWatts :: Maybe Int
  , summaryActivityWeightedAverageWatts :: Maybe Int
  }
  deriving (Show, Generic)

instance FromJSON SummaryActivity where
  parseJSON = genericStravaAPIJSON 15

data SummaryAthlete = SummaryAthlete
  { summaryAthleteFirstname :: String
  , summaryAthleteLastname :: String
  , summaryAthleteProfileMedium :: Maybe String
  , summaryAthleteProfile :: Maybe String
  , summaryAthleteCity :: Maybe String
  , summaryAthleteState :: Maybe String
  , summaryAthleteCountry :: Maybe String
  , summaryAthleteSex :: Maybe Sex
  , summaryAthleteSummit :: Maybe Bool
  , summaryAthleteCreated :: Maybe UTCTime
  , summaryAthleteUpdated :: Maybe UTCTime
  }
  deriving (Show, Generic)

instance FromJSON SummaryAthlete where
  parseJSON = genericStravaAPIJSON 14

data Athlete = Athlete
  { athleteId :: Maybe AthleteId
  , athleteSummary :: Maybe SummaryAthlete
  , athleteDetailed :: Maybe DetailedAthlete
  }
  deriving (Show)

instance FromJSON Athlete where
  parseJSON =
    genericResourceDependent
      "Athlete"
      Athlete
      (.:? "id")

data SummaryClub = SummaryClub
  { summaryClubName :: String
  , summaryClubProfileMedium :: String
  , summaryClubCoverPhoto :: Maybe String
  , summaryClubCoverPhotoSmall :: Maybe String
  , summaryClubSportType :: SportType
  , summaryClubCity :: String
  , summaryClubState :: String
  , summaryClubCountry :: String
  , summaryClubPrivate :: Bool
  , summaryClubMemberCount :: Integer
  , summaryClubFeatured :: Bool
  , summaryClubVerified :: Bool
  , summaryClubUrl :: String
  }
  deriving (Show, Generic)

instance FromJSON SummaryClub where
  parseJSON = genericStravaAPIJSON 11

data TemperatureStream

data TimeStream

data TimedZoneRange = TimedZoneRange
  { timedZoneRangeMin :: Integer
  , timedZoneRangeMax :: Integer
  , timedZoneRangeTime :: NominalDiffTime
  }
  deriving (Show, Generic)

instance FromJSON TimedZoneRange where
  parseJSON = genericStravaAPIJSON 14

data Activity = Activity
  { activityId :: ActivityId
  , activitySummary :: Maybe SummaryActivity
  , activityDetailed :: Maybe DetailedActivity
  }
  deriving (Show, Generic)

instance FromJSON Activity where
  parseJSON =
    genericResourceDependent
      "Activity"
      Activity
      (.: "id")

type DetailedActivityJSON = DetailedActivityF (Maybe [SegmentEffort])

type DetailedActivity = DetailedActivityF [SegmentEffort]

data DetailedActivityF a = DetailedActivity
  { detailedActivityDescription :: Maybe String
  , detailedActivityPhotos :: PhotosSummary
  , detailedActivityGear :: Maybe Gear
  , detailedActivityCalories :: Double
  , detailedActivitySegmentEfforts :: [SegmentEffort]
  , detailedActivityDeviceName :: Maybe String
  , detailedActivityEmbedToken :: String
  , detailedActivitySplitsMetric :: Maybe [Split]
  , detailedActivitySplitsStandard :: Maybe [Split]
  , detailedActivityLaps :: Maybe [Lap]
  , detailedActivityBestEfforts :: a
  }
  deriving (Show, Generic)

instance FromJSON DetailedActivity where
  parseJSON v = do
    da <- genericStravaAPIJSON 16 v
    return $ da{detailedActivityBestEfforts = fromMaybe [] $ detailedActivityBestEfforts da}

data DetailedAthlete = DetailedAthlete
  { detailedAthleteFollowerCount :: Integer
  , detailedAthleteFriendCount :: Integer
  , detailedAthleteMeasurementPreference :: Unit
  , detailedAthleteFtp :: Integer
  , detailedAthleteWeight :: Double
  , detailedAthleteClubs :: [Club]
  , detailedAthleteBikes :: [Gear]
  , detailedAthleteShoes :: [SummaryGear]
  }
  deriving (Show, Generic)

instance FromJSON DetailedAthlete where
  parseJSON = genericStravaAPIJSON 15

data SportType = Cycling | Running | Triathlon | Other
  deriving (Show)

instance FromJSON SportType where
  parseJSON = withText "SportType" $ \case
    "cycling" -> pure Cycling
    "running" -> pure Running
    "triathlon" -> pure Triathlon
    "other" -> pure Other
    _ -> mempty

data Membership = Member | Pending
  deriving (Show)

instance FromJSON Membership where
  parseJSON = withText "Membership" $ \case
    "member" -> pure Member
    "pending" -> pure Pending
    _ -> mempty

data Club = Club
  { clubId :: ClubId
  , clubSummary :: Maybe SummaryClub
  , clubDetailed :: Maybe DetailedClub
  }
  deriving (Show)

instance FromJSON Club where
  parseJSON =
    genericResourceDependent
      "Club"
      Club
      (.: "id")

data DetailedClub = DetailedClub
  { detailedClubMembership :: Maybe Membership
  , detailedClubAdmin :: Bool
  , detailedClubOwner :: Bool
  , detailedClubFollowingCount :: Int
  }
  deriving (Show, Generic)

instance FromJSON DetailedClub where
  parseJSON = genericStravaAPIJSON 12

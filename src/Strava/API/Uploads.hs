{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Strava.API.Uploads (
  getUploadById,
) where

import Polysemy
import Strava.API
import Strava.APITypes

createUpload :: (Members '[Strava] r) => Sem r Upload
createUpload = (error "implement createUpload")

getUploadById :: (Members '[Strava] r) => UploadId -> Sem r Upload
getUploadById uId = do
  let uri =
        makeAPIURI
          ("/uploads/" <> asPathParam uId)
          []
  getJSON uri

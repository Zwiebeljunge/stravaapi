{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Strava.API.Routes (
  getRouteAsGPX,
  getRouteAsTCX,
  getRouteById,
  getRoutesByAthleteId,
) where

import Data.ByteString.Lazy (ByteString)
import Polysemy
import Strava.API
import Strava.APITypes

getRouteAsGPX :: (Members '[Strava] r) => RouteId -> Sem r ByteString
getRouteAsGPX routeId = do
  let uri = makeAPIURI ("/routes/" <> asPathParam routeId <> "/export_gpx") []
  getBS uri

getRouteAsTCX :: (Members '[Strava] r) => RouteId -> Sem r ByteString
getRouteAsTCX routeId = do
  let uri = makeAPIURI ("/routes/" <> asPathParam routeId <> "/export_tcx") []
  getBS uri

getRouteById :: (Members '[Strava] r) => RouteId -> Sem r Route
getRouteById routeId = do
  let uri = makeAPIURI ("/routes/" <> asPathParam routeId) []
  getJSON uri

getRoutesByAthleteId :: (Members '[Strava] r) => AthleteId -> Maybe Int -> Maybe Int -> Sem r [Route]
getRoutesByAthleteId athleteId page perPage = do
  let uri =
        makeAPIURI
          ("/athletes/" <> asPathParam athleteId <> "/routes")
          [ page `asParam` "page"
          , perPage `asParam` "per_page"
          ]
  getJSON uri

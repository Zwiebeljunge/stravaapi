{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Strava.API.Gears (
  gearById,
) where

import Polysemy
import Strava.API
import Strava.APITypes

gearById :: (Members '[Strava] r) => String -> Sem r Gear
gearById gearId = do
  let uri = makeAPIURI ("/gear/" <> asPathParam gearId) []
  getJSON uri

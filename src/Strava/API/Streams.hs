{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Strava.API.Streams (
  getActivityStreams,
) where

import Polysemy
import Strava.API
import Strava.APITypes

getActivityStreams :: (Members '[Strava] r) => ActivityId -> Sem r StreamSet
getActivityStreams activityId = do
  let uri =
        makeAPIURI
          ("/activities/" <> asPathParam activityId <> "/streams")
          [ [ TimeStream
            , DistanceStream
            , LatLngStream
            , AltitudeStream
            , VelocitySmoothStream
            , HeartrateStream
            , CadenceStream
            , WattsStream
            , TempStream
            , MovingStream
            , GradeSmoothStream
            ]
              `asParam` "keys"
              -- False `asParam` "key_by_type"
          ]
  getJSON uri

getRouteStreams :: Sem r a
getRouteStreams = (error "implement getRouteStreams")

getSegmentEffortStreams :: Sem r a
getSegmentEffortStreams = (error "implement getSegementEffortStreams")

getSegmentStreams :: Sem r a
getSegmentStreams = (error "implement getSegmentStreams")

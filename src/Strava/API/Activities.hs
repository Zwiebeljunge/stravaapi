{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Strava.API.Activities (
  getActivityById,
  getCommentsByActivityId,
  getKudoersByActivityId,
  getLapsByActivityId,
  getLoggedInAthleteActivities,
  getZonesByActivityId,
  updateActivityById,
  updateableActivityFromActivity,
) where

import Data.Maybe (fromMaybe)
import Polysemy
import Strava.API
import Strava.APITypes

createActivity :: Sem r a
createActivity = (error "implement createActivity")

-- | Returns the 'Activity' for the given 'AcitivtyId'
getActivityById :: (Members '[Strava] r) => ActivityId -> Bool -> Sem r Activity
getActivityById activityId includeAllEfforts = do
  let uri =
        makeAPIURI
          ("/activities/" <> asPathParam activityId)
          [includeAllEfforts `asParam` "include_all_efforts"]
  getJSON uri

getCommentsByActivityId :: (Members '[Strava] r) => ActivityId -> Sem r [Comment]
getCommentsByActivityId activityId = do
  let uri = makeAPIURI ("/activities/" <> asPathParam activityId <> "/comments") []
  getJSON uri

getKudoersByActivityId :: (Members '[Strava] r) => ActivityId -> Maybe Int -> Maybe Int -> Sem r [Athlete]
getKudoersByActivityId activityId page perPage = do
  let uri =
        makeAPIURI
          ("/activities/" <> asPathParam activityId <> "/kudos")
          [ page `asParam` "page"
          , perPage `asParam` "per_page"
          ]
  getJSON uri

getLapsByActivityId :: (Members '[Strava] r) => ActivityId -> Sem r [Lap]
getLapsByActivityId activityId = do
  let uri =
        makeAPIURI
          ("/activities/" <> asPathParam activityId <> "/laps")
          []
  getJSON uri

getLoggedInAthleteActivities :: (Members '[Strava] r) => Maybe UTCTime -> Maybe UTCTime -> Maybe Int -> Maybe Int -> Sem r [Activity]
getLoggedInAthleteActivities start end page perPage = do
  let uri =
        makeAPIURI
          "/athlete/activities"
          [ start `asParam` "after"
          , end `asParam` "before"
          , page `asParam` "page"
          , perPage `asParam` "per_page"
          ]
  getJSON uri

getZonesByActivityId :: (Members '[Strava] r) => ActivityId -> Sem r [ActivityZone]
getZonesByActivityId activityId = do
  let uri = makeAPIURI ("/activities/" <> asPathParam activityId <> "/zones") []
  getJSON uri

updateActivityById :: (Members '[Strava] r) => ActivityId -> UpdateableActivity -> Sem r Activity
updateActivityById activityId updateableActivity = do
  let uri = makeAPIURI ("/activities/" <> asPathParam activityId) []
  putJSON uri updateableActivity

updateableActivityFromActivity :: Activity -> UpdateableActivity
updateableActivityFromActivity act =
  UpdateableActivity
    (fromMaybe False $ summaryActivityCommute =<< activitySummary act)
    (fromMaybe False $ summaryActivityTrainer =<< activitySummary act)
    (fromMaybe False $ summaryActivityHideFromHome =<< activitySummary act)
    (detailedActivityDescription =<< activityDetailed act)
    (summaryActivityName <$> activitySummary act)
    (summaryActivityType <$> activitySummary act)
    (gearIdToUpdateableGear $ summaryActivityGearId =<< activitySummary act)
  where
    gearIdToUpdateableGear Nothing = None
    gearIdToUpdateableGear (Just gearId) = NewGearId gearId

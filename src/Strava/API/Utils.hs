{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE RecordWildCards #-}

module Strava.API.Utils (
  allAthleteActivitiesSince,
  getDetailedActivities,
  getPowerCurve,
  PowerCurve,
  lookupPower,
  generatePowerCurve,
  reconstructGPX,
  reconstructGPXData,
  getGPXData,
  gpxFileFromByteString,
) where

import qualified Codec.Compression.Zlib as Zlib
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.UTF8 as UTF8
import Data.Function
import Data.List
import Data.Maybe
import Data.Time
import qualified Effects.Logging as Log
import GHC.Generics
import Polysemy
import Strava.API
import Strava.API.Activities
import Strava.API.Streams
import Strava.APITypes

allAthleteActivitiesSince :: (Members '[Strava] r) => UTCTime -> Sem r [Activity]
allAthleteActivitiesSince start = go 1 []
  where
    actsPerPage :: Int
    actsPerPage = 200
    go :: (Members '[Strava] r) => Int -> [Activity] -> Sem r [Activity]
    go page acc = do
      acts <- getLoggedInAthleteActivities (Just start) Nothing (Just page) (Just actsPerPage)
      let newAcc = acc <> acts
      if length acts < actsPerPage
        then pure newAcc
        else go (page + 1) newAcc

getDetailedActivities :: (Members '[Strava] r) => [ActivityId] -> Sem r [Activity]
getDetailedActivities =
  mapM (`getActivityById` False)

data PowerCurve = PowerCurve
  { powerCurvePoints :: [(Int, Double)]
  , powerCurveLimit :: Double
  }
  deriving (Show, Generic)

powerCurve :: [(Int, Double)] -> Maybe PowerCurve
powerCurve [] = Nothing
powerCurve entries = Just $ PowerCurve (sortBy (compare `on` fst) entries) (0.9 * minimum (map snd entries))

lookupPower :: PowerCurve -> Int -> Double
lookupPower (PowerCurve entries limit) int = interpolate relevantInterval
  where
    relevantInterval = case span ((< int) . fst) entries of
      ([], _) -> Nothing
      (_, []) -> Nothing
      (xs, y : _) -> Just (last xs, y)

    interpolate :: Maybe ((Int, Double), (Int, Double)) -> Double
    interpolate = \case
      Nothing -> limit
      Just ((t1, p1), (t2, p2)) -> p1 + (p2 - p1) / fromIntegral (t2 - t1) * fromIntegral (int - t1)

getPowerCurve :: (Members '[Strava] r) => ActivityId -> Sem r (Maybe PowerCurve)
getPowerCurve = fmap generatePowerCurve . getActivityStreams

generatePowerCurve :: StreamSet -> Maybe PowerCurve
generatePowerCurve StreamSet{..} = do
  time <- timeStream
  power <- wattsStream
  generatePowerCurve' time power
  where
    generatePowerCurve' :: Stream Double -> Stream Double -> Maybe PowerCurve
    generatePowerCurve' time power =
      let timeIntervalStream = map (uncurry (-)) $ zipStreamsWithOffset 1 time time
          timedPowerStream = zip timeIntervalStream (streamData power)
          activityLength = floor $ sum timeIntervalStream
          targetTimes = takeWhile (<= activityLength) [scale * 60 ^ unit | unit <- [(0 :: Int) ..], scale <- [1, 3, 5, 10, 15, 20, 30, 45]]
       in powerCurve $ map (\int -> (int, findMaxPower int timedPowerStream)) targetTimes
      where
        findMaxPower :: Int -> [(Double, Double)] -> Double
        findMaxPower int =
          maximum
            . map snd
            . filter (\(wSum, _) -> round wSum >= int)
            . map (takeUntil 0 0)
            . tails
            . filter (not . isNaN . snd)
          where
            takeUntil wSum prodSum [] = (wSum, prodSum / wSum)
            takeUntil wSum prodSum ((w, p) : xs)
              | round wSum >= int = (wSum, prodSum / wSum)
              | otherwise = takeUntil (wSum + w) (prodSum + w * p) xs

zipStreamsWithOffset :: Int -> Stream a -> Stream b -> [(a, b)]
zipStreamsWithOffset off s1 s2 =
  zip
    (drop off $ streamData s1)
    (streamData s2)

data TrkPoint = TrkPoint
  { lat :: Double
  , lng :: Double
  , time :: Maybe UTCTime
  , elevation :: Maybe Double
  , power :: Maybe Int
  , heartrate :: Maybe Int
  , cadence :: Maybe Int
  }

generateGPXData :: UTCTime -> String -> ActivityType -> [TrkPoint] -> String
generateGPXData startTime name actType !points =
  concat $
    prefix
      <> concatMap renderTrkpt points
      <> suffix
  where
    prefix =
      [ "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
      , "<gpx xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd\" creator=\"StravaGPX\" version=\"1.1\" xmlns=\"http://www.topografix.com/GPX/1/1\" xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\" xmlns:gpxx=\"http://www.garmin.com/xmlschemas/GpxExtensions/v3\">"
      , "<metadata>"
      , "<time>" <> formatTime defaultTimeLocale "%FT%TZ" startTime <> "</time>"
      , "</metadata>"
      , "<trk>"
      , "<name>" <> name <> "</name>"
      , "<type>" <> show actType <> "</type>"
      , "<trkseg>"
      ]
    suffix =
      [ "</trkseg>"
      , "</trk>"
      , "</gpx>"
      ]

    hasExtensions :: TrkPoint -> Bool
    hasExtensions trkpt =
      isJust trkpt.power
        || hasTrkptExtension trkpt

    hasTrkptExtension :: TrkPoint -> Bool
    hasTrkptExtension trkpt =
      isJust trkpt.heartrate
        || isJust trkpt.cadence

    renderTrkpt :: TrkPoint -> [String]
    renderTrkpt trkpt =
      ["<trkpt lat=\"" <> show trkpt.lat <> "\" lon=\"" <> show trkpt.lng <> "\">"]
        <> case trkpt.elevation of
          Nothing -> []
          Just elevation -> ["<ele>" <> show elevation <> "</ele>"]
        <> case trkpt.time of
          Nothing -> []
          Just time -> ["<time>" <> formatTime defaultTimeLocale "%FT%TZ" time <> "</time>"]
        <> ( if hasExtensions trkpt
              then
                ["<extensions>"]
                  <> case trkpt.power of
                    Nothing -> []
                    Just power -> ["<power>" <> show power <> "</power>"]
                  <> ( if hasTrkptExtension trkpt
                        then
                          ["<gpxtpx:TrackPointExtension>"]
                            <> ( case trkpt.heartrate of
                                  Nothing -> []
                                  Just heartrate -> ["<gpxtpx:hr>" <> show heartrate <> "</gpxtpx:hr>"]
                               )
                            <> ( case trkpt.cadence of
                                  Nothing -> []
                                  Just cadence -> ["<gpxtpx:cad>" <> show cadence <> "</gpxtpx:cad>"]
                               )
                            <> ["</gpxtpx:TrackPointExtension>"]
                        else []
                     )
                  <> ["</extensions>"]
              else []
           )
        <> ["</trkpt>"]

reconstructGPX :: (Members '[Strava, Log.Log, Embed IO] r) => FilePath -> ActivityId -> Sem r ()
reconstructGPX gpxPath actId = do
  mGpx <- getGPXData actId
  case mGpx of
    Nothing -> Log.logFatal $ "Could not reconstruct GPX for " <> show actId
    Just gpx -> embed $ writeFile gpxPath gpx

getGPXData :: (Members '[Strava, Log.Log, Embed IO] r) => ActivityId -> Sem r (Maybe String)
getGPXData actId = do
  streamSet <- getActivityStreams actId
  act <- getActivityById actId False
  case ( streamSet.latlngStream
       , act.activitySummary >>= summaryActivityStartDate
       , summaryActivityName <$> act.activitySummary
       , summaryActivityType <$> act.activitySummary
       ) of
    (Just coords, Just startTime, Just name, Just actType) -> do
      let times = streamData . fmap (\off -> addUTCTime (fromInteger $ round off) startTime) <$> streamSet.timeStream
      let altitudes = streamData <$> streamSet.altituteStream
      let watts = map round . streamData <$> streamSet.wattsStream
      let heartrates = map round . streamData <$> streamSet.heartrateStream
      let cadences = map round . streamData <$> streamSet.cadenceStream
      pure $ Just $ generateGPXData startTime name actType $ constructTrackPoints (streamData coords) times altitudes watts heartrates cadences
    _ -> do
      Log.logFatal "Failed to get time and latlng streams"
      pure Nothing

reconstructGPXData :: (Members '[Strava, Log.Log, Embed IO] r) => ActivityId -> Sem r (Maybe LBS.ByteString)
reconstructGPXData actId = fmap (Zlib.compress . LBS.fromStrict . UTF8.fromString) <$> getGPXData actId

gpxFileFromByteString :: (Members '[Embed IO] r) => FilePath -> LBS.ByteString -> Sem r ()
gpxFileFromByteString path = embed . writeFile path . UTF8.toString . LBS.toStrict . Zlib.decompress

step :: (Maybe a -> b) -> Maybe [a] -> (b, Maybe [a])
step f Nothing = (f Nothing, Nothing)
step f (Just (x : xs)) = (f (Just x), Just xs)
step f (Just []) = (f Nothing, Nothing)

stepRealFloat :: (RealFloat a) => (Maybe a -> b) -> Maybe [a] -> (b, Maybe [a])
stepRealFloat f Nothing = (f Nothing, Nothing)
stepRealFloat f (Just (x : xs))
  | isNaN x || isInfinite x = (f Nothing, Just xs)
  | otherwise = (f (Just x), Just xs)
stepRealFloat f (Just []) = (f Nothing, Nothing)

constructTrackPoints :: [(Double, Double)] -> Maybe [UTCTime] -> Maybe [Double] -> Maybe [Int] -> Maybe [Int] -> Maybe [Int] -> [TrkPoint]
constructTrackPoints [] _ _ _ _ _ = []
constructTrackPoints ((lat, lng) : coords) times altitudes watts heartrates cadences =
  let (nextAlts, restTimes) = step (TrkPoint lat lng) times
      (nextWatts, restAltitudes) = stepRealFloat nextAlts altitudes
      (nextHeartrates, restWatts) = step nextWatts watts
      (nextCadences, restHeartrates) = step nextHeartrates heartrates
      (trkpt, restCadences) = step nextCadences cadences
   in trkpt : constructTrackPoints coords restTimes restAltitudes restWatts restHeartrates restCadences

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators #-}

module Strava.API.WebHookEvents where

import Control.Applicative
import qualified Data.Aeson as Aeson
import Data.Aeson.Lens
import qualified Data.ByteString.Char8 as Char8
import Data.Time
import Lens.Micro
import Network.HTTP.Simple
import Network.HTTP.Types
import Polysemy
import qualified Polysemy.Error as Error

data AspectType
  = Create
  | Update
  | Delete
  deriving (Show)

data ObjectType
  = Athlete
  | Activity
  deriving (Show)

data UpdateContent = UpdateContent
  { updateField :: String
  , update :: String
  }
  deriving (Show)

data WebhookEvent = WebhookEvent
  { aspectType :: AspectType
  , eventTime :: UTCTime
  , objectId :: Int
  , objectType :: ObjectType
  , ownerId :: Int
  , subscriptionId :: Int
  , updates :: [UpdateContent]
  }
  deriving (Show)

newtype SubscriptionId = SubscriptionId {subId :: Int}
  deriving (Show)

data Subscription = Subscription
  { subscriptionId :: SubscriptionId
  , resourceState :: Int
  , applicationId :: Int
  , callbackUrl :: String
  , createdAt :: UTCTime
  , updatedAt :: UTCTime
  }
  deriving (Show)

instance Aeson.FromJSON Subscription where
  parseJSON = Aeson.withObject "Subscription" $ \o ->
    Subscription
      <$> (SubscriptionId <$> o Aeson..: "id")
      <*> (o Aeson..: "resource_state")
      <*> (o Aeson..: "application_id")
      <*> (o Aeson..: "callback_url")
      <*> (o Aeson..: "created_at")
      <*> (o Aeson..: "updated_at")

data StravaWebhookEvents m a where
  CreateSubscription ::
    { callbackURL :: String
    , verifyToken :: String
    } ->
    StravaWebhookEvents m SubscriptionId
  ViewSubscription :: StravaWebhookEvents m [Subscription]
  DeleteSubscription :: {subscriptionId :: SubscriptionId} -> StravaWebhookEvents m Bool

makeSem ''StravaWebhookEvents

runStravaWebHookEvents ::
  (Members '[Embed IO, Error.Error CreateSubscriptionError] r) =>
  String ->
  String ->
  Sem (StravaWebhookEvents : r) a ->
  Sem r a
runStravaWebHookEvents clientId clientSecret = interpret $ \case
  CreateSubscription{..} -> Error.fromEitherM $ runCreateSubscription clientId clientSecret callbackURL verifyToken
  ViewSubscription -> embed $ runViewSubscription clientId clientSecret
  DeleteSubscription{..} -> embed $ runDeleteSubscription clientId clientSecret subscriptionId

subscriptionsUrl :: String
subscriptionsUrl = "https://www.strava.com/api/v3/push_subscriptions"

(.=) :: a -> b -> (a, b)
(.=) = (,)

runViewSubscription :: String -> String -> IO [Subscription]
runViewSubscription clientId clientSecret = do
  let req =
        setRequestQueryString
          [ "client_id" .= Just (Char8.pack clientId)
          , "client_secret" .= Just (Char8.pack clientSecret)
          ]
          $ setRequestMethod "GET"
          $ parseRequest_ subscriptionsUrl

  either (error . show) id . Aeson.eitherDecode' . getResponseBody <$> httpLBS req

newtype CreateSubscriptionResponse = CreateSubscriptionResponse
  { unCreateSubscriptionResponse :: Either CreateSubscriptionError SubscriptionId
  }

data CreateSubscriptionError
  = CallbackURLNo200
  | MalformedChallengeReponse
  deriving (Show)

instance Aeson.FromJSON CreateSubscriptionError where
  parseJSON v = do
    let errorField = v ^? key "errors" . _Array . traverse . key "field" . _String
        errorCode = v ^? key "errors" . _Array . traverse . key "code" . _String
    case (errorField, errorCode) of
      (Just "callback url", Just "GET to callback URL does not return 200") -> pure CallbackURLNo200
      (Just "challenge response", Just "challenge response malformed") -> pure MalformedChallengeReponse
      (Nothing, _) -> mempty
      _ -> error $ "cannot parse: " <> show v

instance Aeson.FromJSON CreateSubscriptionResponse where
  parseJSON v =
    Aeson.withObject
      "CreateSubscriptionResponse"
      ( \o ->
          CreateSubscriptionResponse
            <$> ( Right . SubscriptionId
                    <$> o
                      Aeson..: "id"
                    <|> (Left <$> Aeson.parseJSON v)
                )
      )
      v

runCreateSubscription :: String -> String -> String -> String -> IO (Either CreateSubscriptionError SubscriptionId)
runCreateSubscription clientId clientSecret callbackURL verifyToken = do
  let req =
        setRequestBodyURLEncoded
          [ "client_id" .= Char8.pack clientId
          , "client_secret" .= Char8.pack clientSecret
          , "callback_url" .= Char8.pack callbackURL
          , "verify_token" .= Char8.pack verifyToken
          ]
          $ setRequestMethod "POST"
          $ parseRequest_ subscriptionsUrl

  extractResponse . Aeson.decode' . getResponseBody <$> httpLBS req
  where
    extractResponse :: Maybe CreateSubscriptionResponse -> Either CreateSubscriptionError SubscriptionId
    extractResponse Nothing = error "failed to parse response"
    extractResponse (Just resp) = unCreateSubscriptionResponse resp

runDeleteSubscription :: String -> String -> SubscriptionId -> IO Bool
runDeleteSubscription clientId clientSecret (SubscriptionId sId) = do
  let req =
        setRequestQueryString
          [ "client_id" .= Just (Char8.pack clientId)
          , "client_secret" .= Just (Char8.pack clientSecret)
          ]
          $ setRequestMethod "DELETE"
          $ parseRequest_
          $ subscriptionsUrl <> "/" <> show sId
  status <- getResponseStatus <$> httpBS req
  pure $ status == status204

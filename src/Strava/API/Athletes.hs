{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Strava.API.Athletes (
  getLoggedInAthlete,
  getLoggedInAthleteZones,
  getStats,
) where

import Polysemy
import Strava.API
import Strava.APITypes

getLoggedInAthlete :: (Members '[Strava] r) => Sem r Athlete
getLoggedInAthlete = do
  let uri = makeAPIURI "/athlete" []
  getJSON uri

getLoggedInAthleteZones :: (Members '[Strava] r) => Sem r Zones
getLoggedInAthleteZones = do
  let uri = makeAPIURI "/athlete/zones" []
  getJSON uri

getStats :: (Members '[Strava] r) => AthleteId -> Sem r ActivityStats
getStats athleteId = do
  let uri = makeAPIURI ("/athletes/" <> asPathParam athleteId <> "/stats") []
  getJSON uri

updateLoggedInAthlete :: Sem r a
updateLoggedInAthlete = (error "implement updateLoggedInAthlete")

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Strava.API.SegmentEfforts (
  getEffortsBySegmentId,
  getSegmentEffortById,
) where

import Polysemy
import Strava.API
import Strava.APITypes

getEffortsBySegmentId :: (Members '[Strava] r) => SegmentId -> Maybe UTCTime -> Maybe UTCTime -> Maybe Int -> Sem r [SegmentEffort]
getEffortsBySegmentId segmentId start end perPage = do
  let uri =
        makeAPIURI
          "/segment_efforts"
          [ segmentId `asParam` "segment_id"
          , start `asParam` "start_date_local"
          , end `asParam` "end_date_local"
          , perPage `asParam` "per_page"
          ]
  getJSON uri

getSegmentEffortById :: (Members '[Strava] r) => SegmentId -> Sem r SegmentEffort
getSegmentEffortById segmentId = do
  let uri =
        makeAPIURI
          ("/segment_efforts/" <> asPathParam segmentId)
          []
  getJSON uri

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Strava.API.Segments (
  getLoggedInAthleteStarredSegments,
  getSegmentById,
) where

import Polysemy
import Strava.API
import Strava.APITypes

exploreSegments :: a
exploreSegments = (error "implement exploreSegments")

getLoggedInAthleteStarredSegments :: (Members '[Strava] r) => Maybe Int -> Maybe Int -> Sem r [SummarySegment]
getLoggedInAthleteStarredSegments page perPage = do
  let uri =
        makeAPIURI
          "/segments/starred"
          [ page `asParam` "page"
          , perPage `asParam` "per_page"
          ]
  getJSON uri

getSegmentById :: (Members '[Strava] r) => SegmentId -> Sem r DetailedSegment
getSegmentById segmentId = do
  let uri =
        makeAPIURI
          ("/segments/" <> asPathParam segmentId)
          []
  getJSON uri

starSegment :: Sem r a
starSegment = (error "implement starSegment")

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Strava.API.Clubs (
  getClubActivitiesById,
  getClubAdminsById,
  getClubById,
  getClubMembersById,
  getLoggedInAthleteClubs,
) where

import Polysemy
import Strava.API
import Strava.APITypes

getClubActivitiesById :: (Members '[Strava] r) => ClubId -> Maybe Int -> Maybe Int -> Sem r [Activity]
getClubActivitiesById clubId page perPage = do
  let uri =
        makeAPIURI
          ("/clubs/" <> asPathParam clubId <> "/activities")
          [ page `asParam` "page"
          , perPage `asParam` "per_page"
          ]
  getJSON uri

getClubAdminsById :: (Members '[Strava] r) => ClubId -> Maybe Int -> Maybe Int -> Sem r [Athlete]
getClubAdminsById clubId page perPage = do
  let uri =
        makeAPIURI
          ("/clubs/" <> asPathParam clubId <> "/admins")
          [ page `asParam` "page"
          , perPage `asParam` "per_page"
          ]
  getJSON uri

getClubById :: (Members '[Strava] r) => ClubId -> Sem r Club
getClubById clubId = do
  let uri = makeAPIURI ("/clubs/" <> asPathParam clubId) []
  getJSON uri

getClubMembersById :: (Members '[Strava] r) => ClubId -> Maybe Int -> Maybe Int -> Sem r [Athlete]
getClubMembersById clubId page perPage = do
  let uri =
        makeAPIURI
          ("/clubs/" <> asPathParam clubId <> "/members")
          [ page `asParam` "page"
          , perPage `asParam` "per_page"
          ]
  getJSON uri

getLoggedInAthleteClubs :: (Members '[Strava] r) => Maybe Int -> Maybe Int -> Sem r [Club]
getLoggedInAthleteClubs page perPage = do
  let uri =
        makeAPIURI
          "/athlete/clubs"
          [ page `asParam` "page"
          , perPage `asParam` "per_page"
          ]
  getJSON uri

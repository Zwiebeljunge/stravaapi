{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings #-}

module Strava.Types (
  Fault (..),
  Error (..),
  specifyFaultLocation,
  ResourceNotFound (..),
) where

import Data.Aeson hiding (Error)
import Data.Maybe

data Error = Error
  { errorResource :: String
  , errorField :: String
  , errorCode :: String
  }
  deriving (Show)

instance FromJSON Error where
  parseJSON = withObject "Error" $ \o ->
    Error
      <$> o .: "resource"
      <*> o .: "field"
      <*> o .: "code"

data Fault = Fault
  { faultErrors :: [Error]
  , faultMessage :: String
  }
  deriving (Show)

specifyFaultLocation :: String -> Fault -> Fault
specifyFaultLocation loc f = f{faultMessage = loc <> ": '" <> f.faultMessage <> "'"}

instance FromJSON Fault where
  parseJSON = withObject "Fault" $ \o ->
    Fault
      <$> (fromMaybe [] <$> o .:? "errors")
      <*> o .: "message"

data ResourceNotFound
  = ResourceNotFound
  deriving (Show)

{ inputs, system }:

let
  overlays = import ./overlays.nix { inherit inputs; };
  config = { };
  pkgs = import inputs.nixpkgs { inherit config overlays system; };
in pkgs

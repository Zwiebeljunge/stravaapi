{ inputs }:
let
  customHaskellPackages = self: super: {
    haskellPackages = super.haskellPackages.override {
      overrides = hself: hsuper:
        let
          dontCheck = super.haskell.lib.dontCheck;
          dontHaddock = super.haskell.lib.dontHaddock;

          # Disable Haddock generation and profiling by default. The former
          # can be done via cabal, while the latter should be enabled on
          # demand.
          defaultMod = drv:
            super.haskell.lib.dontHaddock
            (super.haskell.lib.disableLibraryProfiling drv);

          polysemy-utils = defaultMod
            (hself.callCabal2nix "polysemy-utils" inputs.polysemy-utilsSrc { });

          strava-api-src = self.nix-gitignore.gitignoreSource [
            "*.git"
            "dist"
            "dist-newstyle"
          ] ../.;
          strava-api = hself.callCabal2nix "strava-api" strava-api-src { };
        in {
          # We add ourselves to the set of haskellPackages.
          inherit strava-api;
          inherit polysemy-utils;
        };
    };
  };
in [ customHaskellPackages ]

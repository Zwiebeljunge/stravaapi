# Changelog

[Strava Changelog](https://developers.strava.com/docs/changelog/)

## 29.12.2021 Remove RunningRace Endpoint

Strava removed running races in October.

## 16.01.2022 Adds hide_from_homde

Adds field hide_from_home to SummaryActivity and UpdateableActivity
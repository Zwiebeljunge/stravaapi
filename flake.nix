{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    polysemy-utilsSrc.url = "github:lzszt/polysemy-utils";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs =
    inputs@{ self, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import ./nix/pkgs.nix { inherit inputs system; };
        packageName = "strava-api";
      in
      {
        packages.${packageName} = pkgs.haskellPackages.${packageName};
        defaultPackage = self.packages.${system}.${packageName};
        devShells = {
          default = pkgs.haskellPackages.shellFor {
            packages = p: [ p.${packageName} ];
            nativeBuildInputs = with pkgs; [
              haskellPackages.cabal-install
              haskellPackages.ghc
              haskellPackages.hlint
              haskellPackages.ghcid
              haskellPackages.haskell-language-server
              haskellPackages.fourmolu
              haskellPackages.cabal-fmt
            ];
          };
        };
      }
    );
}
